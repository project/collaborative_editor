function addVisualElements() {
	var i = 0, el, els = document.getElementsByTagName("fieldset");
	// iterates each input element
	while (el = els[i++]) {
		if (el.childNodes[0].childNodes[0].nodeValue == 'Collaborative editor') {
			var fieldSetEl = el;
		}
	}
	var saveMessageEl = document.createElement("DIV");
	fieldSetEl.appendChild(saveMessageEl);
	saveMessageEl.setAttribute("id","autosave");
	saveMessageEl.innerHTML = "Doc not saved yet";
	styleElement(saveMessageEl,'#888','Trebuchet MS','normal','0.9em');

	var helpMessageEl = document.createElement("DIV");
	fieldSetEl.appendChild(helpMessageEl);
	helpMessageEl.innerHTML = "Click on submit button to save the document manually";
	styleElement(helpMessageEl,'#000','Trebuchet MS','normal','0.8em');

	var listHeaderEl = document.createElement("DIV");
	fieldSetEl.appendChild(listHeaderEl);
	listHeaderEl.innerHTML = "Users:";
	styleElement(listHeaderEl,'#000','Trebuchet MS','normal','1em');
	listHeaderEl.style.paddingTop = '10px';
	
	var userListEl = document.createElement("DIV");
	fieldSetEl.appendChild(userListEl);
	userListEl.setAttribute("id","userlist");
}


function showLastUpdate(isCollision, exitDocument) {
	setTimeout(function () {
		if (isCollision != 1 && !exitDocument) {
			var autoSaveEl = document.getElementById("autosave");
			var dateObj = new Date();
			autoSaveEl.innerHTML = "Last saved at " + dateObj.toLocaleTimeString();
			styleElement(autoSaveEl,'#888','Trebuchet MS','normal','1em');
		}	
		}, 2000);
	}


	function showUserList(userList) {
		var userListInitEl = document.getElementById("userlist");
		var userListEl = document.createElement("DIV");
		userListEl.setAttribute("id","userlist");
		userListInitEl.parentNode.replaceChild(userListEl,userListInitEl);
		var newUserListEl = document.getElementById("userlist");
		for (var i = 0; i < userList.length; i++) {
			var newUserEl = document.createElement("LI");
			newUserListEl.appendChild(newUserEl);
			styleElement(newUserListEl,'red','Trebuchet MS','normal','1em');
			newUserListEl.style.listStyle = 'none';
			newUserEl.innerHTML = userList[i] + "<span style='color: #888;'> is editing...</span>";
		}
	}


	function showSaveMessage() {
		var autoSaveEl = document.getElementById("autosave");
		autoSaveEl.innerHTML = "AutoSaving...";
		styleElement(autoSaveEl,'#639ACE','Trebuchet MS','bold','1em');
	}
	
	function styleElement(el,inputColor,inputFontFamily,inputFontWeight,inputFontSize) {
		el.style.color = inputColor;
		el.style.fontFamily = inputFontFamily;
		el.style.fontWeight = inputFontWeight;
		el.style.fontSize = inputFontSize;
	}