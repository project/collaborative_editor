<?php
// $Id: collaborative_editor.module,v 0.10

/**
 * @file
 * Collaborative Editor Module
 *
 * This module allows concurrent edition when content is created in Drupal
 * 
 *
 * IMPORTANT: Read INSTALL.txt first
 *
 *
 */


/**
 * Implementation of hook_help().
 */
 
function collaborative_editor_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return('Allows concurrent edition when content is created');
  }
}


/**
 * Implementation of hook_perm().
 */
function collaborative_editor_perm() {
  return array('create content', 'edit own content');
}


/**
 * Implementation of hook_access().
 */
function collaborative_editor_access($op, $node) {
  global $user;

  if ($op == 'create') {
    return user_access('create content');
  }

  if ($op == 'update' || $op == 'delete') {
    if (user_access('create content') ) {
      return TRUE;
    }
  }
}

/**
 * Implementation of hook_menu
 */
function collaborative_editor_menu($may_cache) {
  $items = array();
  if ($may_cache) {
     /* only used to remove change the user status when they leave the document
     $items[] = array('path' => 'collaborative_editor/removeUserStatus', 
    	'title' => t('collaborative_editor'),
    	'callback' => 'remove_user_status',
    	'type' => MENU_CALLBACK,
      'access' => user_access('create content'));
     */
   }
   else {
		// this path will be called in each async call
   	$items[] = array('path' => 'collaborative_editor/'. arg(1) .'/getAsyncContent',
    	'title' => t('collaborative_editor'),
    	'callback' => 'collaborative_editor_async_content',
    	'type' => MENU_CALLBACK,
      'access' => user_access('create content'));
  }
  return $items;
}




/**
 * Implementation of hook_form_alter
 */
function collaborative_editor_form_alter($form_id, &$form) {
	global $user;
	if (is_numeric(arg(1))) {
		$result = db_query('SELECT * FROM {ce_content} WHERE cid = %d',arg(1));
	  // if we already have the ce content created already
		if (db_num_rows($result)) {
			$content_from_db = db_fetch_object($result);
			$init_content = $content_from_db->body;
			$revision = $content_from_db->rid;
		}
		else {
			$revision = 0;
			$init_content = '';
		}
		
	  if (isset($form['type'])) {
		  	$path = drupal_get_path('module', 'collaborative_editor');
		  	// here we insert the js files we need
		  	drupal_add_js($path . '/drupal.collaborative_editor_ui.js');
		  	drupal_add_js($path . '/drupal.collaborative_editor.js');
	  
			// this fieldset is used to insert ui elements via DOM
		  $form['track'] = array(
				'#type' => 'fieldset',
				'#title' => t('Collaborative editor'),
	  	);
		  $form['cid'] = array('#type' => 'hidden', '#default_value' => arg(1));
		  // the initial revision id should be present in the initial form as well
		  $form['rev'] = array('#type' => 'hidden', '#default_value' => $revision);
		  $form['init'] = array('#type' => 'hidden', '#default_value' => $init_content);
		  // this field is only to show the debug values
		  $form['debug'] = array('#type' => 'textarea', '#title' => t('Debug'), '#rows' => 10);
	  }
	}
}





// this function handles the use cases of each async call
function collaborative_editor_async_content() {
	global $user;
	
	$cid = arg(1);
	
	// remove those user status whose users are not currently editing 
	check_user_status();
	
	//This variable is used to store the revision id the server will send back to the client
	$return_rev = '';
	
	// get post params:
	$new_content = $_POST['diffText'];  // the new content the user has added
	$client_rev = $_POST['clientRev'];  // should be the same as the server give them last time
  
  $result = db_query("SELECT * FROM {ce_content} WHERE cid = %d", $cid);
  // if we already have the ce content created already
	if (db_num_rows($result)) {
		$content_from_db = db_fetch_object($result);
		
		$insert_start = $_POST['prefixLength'];  // in what initial position has been added this content
		$replacement_length = $_POST['replacementLength'];  // if there has been a replacement or content deleted, how long it is
  	
  	// this checks if the client has modified the content
		$is_diff = strlen($new_content) > 0 || $replacement_length != 0;
		
		// insert the values we upload into an array to send it later as pending changes for other users
		// The aim is to avoid other users work on outdated versions of the content
		// This changes will be applied to other users so that they can insert their content in the right place
		$pchanges = array();
		$pchanges['start'] = $insert_start;
		$pchanges['replacement'] = $replacement_length;
		$pchanges['content_length'] = strlen($new_content); // here we dont need to know the content, only its length
		
		// the revision id of the current content on the server. It changes everytime someone uploads new content
		$server_rev = $content_from_db->rid;
		
	}
	else {
		// if there is no ce content we create a new one
		db_query("INSERT INTO {ce_content} (cid,rid) VALUES (%d,'%s')",$cid,$client_rev);
		$server_rev = $client_rev;
		$new_doc = true;
	}
	
  
	/* implement use cases
	# case 1: client uploads diff. no changes in server
	# case 2: client checks but no diff to upload. no changes in server
	# case 3: client uploads diff. there are changes in server (modifications by others)
	# case 4: client checks but no diff to upload. there are changes in server (modifications by others)
	# case 5: client uploads diff but there is a collision
	# case 6: user still typing, whatever other cases are happening. If there are new changes from other users they will be updated when user stops typing
	*/
	if ($server_rev != $client_rev) {  // if the revision in the server is different than ours means that somebody else has done changes in the document
		//we set the flag to know there has been changes from others
		$otherschange = 1;
		if ($is_diff) {  // if we have done any changes
			
			// debug var
			$phase = "#3 - changes done. also changes from others";
			
			// first check if we have pending changes
			$result = db_query("SELECT pchange FROM {ce_pchanges} WHERE uid = %d AND cid = %d", $user->uid, $cid);
			// if so, we use them to know where we will have to insert our content
			while ($row = db_fetch_object($result)) {
				$offset = unserialize($row->pchange); // the values are extracted from the array
				$offset_start = $offset['start'];
				$offset_length = $offset['content_length'];
				$offset_content = $offset['replacement'];
				
				// Note: the more tests we do the more bugs we can find in the lines below of collision detection
				// Dont get confused with the bug # in IE which can make fail the following lines
				// Here we bother the others inserts happening before our caret position. We dont bother those after our caret position
				if ($offset_start <= $insert_start) {
					// check if there is collision with others' changes
					// This is true if our caret is in between others replacements or deletions
					if (($offset_start + $offset_content) > $insert_start) {
						$phase = "#5 - collision";
						$collision = true;
					}
					// update the caret position where we will insert content
					$insert_start += $offset_length - $offset_content;
				}
				
				// there is a second possible collision to check. Others' inserts happen after our caret position
				// This is true if we replace or delete where others' caret is
				elseif (($insert_start + $replacement_length) > $offset_start) {
					$phase = "#5 - collision";
					$collision = true;
				}
			}
			
			// if there's no collision we continue with the use case #3
			if (!$collision) {
				// everytime we store new content the revision id is updated
				$new_rev = rand(10000,99999);
				db_query("UPDATE {ce_content} SET rid = '%s' WHERE cid = %d", $new_rev, $cid);
				
				// put the changes to others' pending list
				// we take the active users from the ce_users table
				$result = db_query("SELECT uid FROM {ce_users} WHERE cid = %d", $cid);
				while ($row = db_fetch_object($result)) {
					if ($row->uid != $user->uid) {
						db_query("INSERT INTO {ce_pchanges} (cid,uid,pchange) VALUES ('%s',%d,'%s')",$cid,$row->uid,serialize($pchanges));
					}
				}
				
				// do the insert or replacement and update the database content
				$display = substr_replace($content_from_db->body, $new_content, $insert_start, $replacement_length); // ( mixed string, string replacement, int start [, int length] )
				db_query("UPDATE {ce_content} SET body = '%s' WHERE cid = %d", $display, $cid);
				
				// delete our pending changes
				delete_pending_changes(arg(1));
				$return_rev = $new_rev;
			} 
			// here we handle the collision case. Since the last change is not valid we only return the server content as it is
			else {
				$display = $content_from_db->body;
				$return_rev = $server_rev;
			
  			delete_pending_changes(arg(1));
  		}
						
		} 
		else {
			$phase = "#4 - no changes. but changes from others";
			
			// we return the content of the server
			$display = $content_from_db->body;
			$return_rev = $server_rev;
			
			delete_pending_changes(arg(1));
		}
		
	}
	// If we get this point is because the revision id in the server is the same as the revision we upload. Therefore there is
	// no change from others in the server content. Two use cases are possible here depending on whether we upload new content (case 1)
	// or not (case 2: periodic check -> when this case happens and we are editing alone the heartbeat refresh ought to be set
	// to 30 seconds or so)
	elseif ($is_diff && !$new_doc) { // #1
		$phase = "#1 - changes done. nobody else changes";
		
		// create a new revision since there is a new version of the content
		$new_rev = rand(10000,99999);
		db_query("UPDATE {ce_content} SET rid = '%s' WHERE cid = %d", $new_rev, $cid);
				
		// put the changes to others' pending list
		// we take the active users from the ce_users table
		$result = db_query("SELECT uid FROM {ce_users} WHERE cid = %d", $cid);
		while ($row = db_fetch_object($result)) {
			if ($row->uid != $user->uid) {
				db_query("INSERT INTO {ce_pchanges} (cid,uid,pchange) VALUES ('%s',%d,'%s')",$cid,$row->uid,serialize($pchanges));
			}
		}
		
		// do changes and update to db
		$display = substr_replace($content_from_db->body, $new_content, $insert_start, $replacement_length); // ( mixed string, string replacement, int start [, int length] )
		db_query("UPDATE {ce_content} SET body = '%s' WHERE cid = %d", $display, $cid);
			
		// delete our pending changes
		delete_pending_changes(arg(1));
		// the revision id returned will be the one we created before
		$return_rev = $new_rev;
		// set the flag since there are no changes from others
		$otherschange = 0;
				
	}
	else {
		// here we dont set the return_rev variable, so this will be blank in the returned hash. The uploaded_rev will be used in the client side instead
		// since there has been no change in the content the uploaded revision id by the client is valid
		$phase = "#2 - no movement";
		$display = $content_from_db->body; // Note: I have to test if I can avoid to send this back
			
		$otherschange = 0;
	}
  
  // set response . json object
  $display = rawurlencode($display); // we encode line breaks since so that json object doesnt show an error
  if ($collision) $collision = 1; // not possible send back boolean types ???


	// we store the current users in an array to show them in the list below the textarea
	$result = db_query("SELECT n.name FROM {users} n, {ce_users} c WHERE c.cid = %d AND c.uid = n.uid", $cid);
	$currentusers = array();
	while ($row = db_fetch_object($result)) {
		array_push($currentusers, "'".$row->name."'");
	}
	$sendusers = implode(",",$currentusers);

	// if the user is editing the content for the first time the user status is set
	$result = db_query('SELECT * FROM {ce_users} WHERE cid = %d AND uid = %d',arg(1),$user->uid);
	if (!db_num_rows($result)) {
		db_query("INSERT INTO {ce_users} (cid,uid) VALUES (%d,%d)",arg(1),$user->uid);
	}
	// if it is not the first time but still editing we update the checked field
	db_query("UPDATE {ce_users} SET checked = %d, refresh = %d WHERE uid = %d AND cid = %d", time(), 20, $user->uid, $cid);
	
  echo "{content: '$display',
  			return_rev: '$return_rev',
  			uploaded_rev: '$client_rev',
  			phase: '$phase',
  			otherschange: '$otherschange',
  			collision: '$collision',
  			currentusers: [ $sendusers ]
  			}";
  

}


function check_user_status() {
	// remove those users who havent edited in the last 20 seconds
  $result = db_query('SELECT * FROM {ce_users} WHERE checked + %d < %d', 20, time());
  while ($row = db_fetch_object($result)) {
  	remove_user_status($row->cid, $row->uid);
  }
}


function delete_pending_changes($cid) {
	global $user;
	
	db_query("DELETE FROM {ce_pchanges} WHERE uid = %d AND cid = %d", $user->uid, $cid);
	
}


function remove_user_status($cid, $uid) {
	global $user;
	
	db_query("DELETE FROM {ce_users} WHERE uid = %d AND cid = %d", $uid, $cid);
	db_query("DELETE FROM {ce_pchanges} WHERE uid = %d AND cid = %d", $uid, $cid);
}

?>
